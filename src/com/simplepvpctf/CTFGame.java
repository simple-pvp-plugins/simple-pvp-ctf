package com.simplepvpctf;

import com.simplepvp.Game;
import com.simplepvp.util.SimpleUtil;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.data.Rotatable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.*;

import java.util.*;

public class CTFGame extends Game {

    // The variables needed for the game.
    private ItemStack joinRedTeamItem;
    private ItemStack joinBlueTeamItem;
    private Player redFlagCarrier = null;
    private Player blueFlagCarrier = null;
    private int redTeamCaptures;
    private int blueTeamCaptures;
    private BukkitTask particleTask;
    private final Team redTeam;
    private final Team blueTeam;
    private final Scoreboard gameScoreBoard;
    private final Objective gameScoreBoardObjective;
    private Score prevRedFlagScore;
    private Score prevBlueFlagScore;
    private final List<Player> redTeamPlayerQueue = new ArrayList<>();
    private final List<Player> blueTeamPlayerQueue = new ArrayList<>();
    private final HashSet<Player> redTeamPlayers = new HashSet<>();
    private final HashSet<Player> blueTeamPlayers = new HashSet<>();
    private final Random random = new Random(System.currentTimeMillis());
    private final CTFMapConfig ctfMapConfig = ((CTFMapConfig) getMapConfig());

    // Initialise the game.
    public CTFGame(String gameName, Material gameIcon, CTFMapConfig mapConfig) {
        super(gameName, gameIcon, mapConfig); // Call the super.
        generateTeamJoinItems();;
        redTeam = createRedTeam();
        blueTeam = createBlueTeam();

        gameScoreBoard = Objects.requireNonNull(Bukkit.getScoreboardManager()).getNewScoreboard();
        gameScoreBoardObjective = gameScoreBoard.registerNewObjective("header", "dummy", ChatColor.RED + ChatColor.BOLD.toString() + "Capture The Flag  ");
        setupScoreBoard();
        resetGame(); // Generate flags if crash occurred.
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Setup Methods
----------------------------------------------------------------------------------------------------------------------*/

    private void generateTeamJoinItems() {
        ItemStack redItem = new ItemStack(Material.RED_CONCRETE);
        ItemMeta redItemMeta = redItem.getItemMeta();
        assert redItemMeta != null;
        redItemMeta.setDisplayName(ChatColor.RED + "Join Red Team");
        redItem.setItemMeta(redItemMeta);
        joinRedTeamItem = redItem;

        ItemStack blueItem = new ItemStack(Material.BLUE_CONCRETE);
        ItemMeta blueItemMeta = blueItem.getItemMeta();
        assert blueItemMeta != null;
        blueItemMeta.setDisplayName(ChatColor.BLUE + "Join Blue Team");
        blueItem.setItemMeta(blueItemMeta);
        joinBlueTeamItem = blueItem;
    }

    private Team createRedTeam() {
        Team team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().getTeam("CTF_RED_TEAM");

        if (team == null) {
            team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().registerNewTeam("CTF_RED_TEAM");
        }

        team.setColor(ChatColor.RED);

        return team;
    }

    private Team createBlueTeam() {
        Team team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().getTeam("CTF_BLUE_TEAM");

        if (team == null) {
            team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().registerNewTeam("CTF_BLUE_TEAM");
        }

        team.setColor(ChatColor.BLUE);

        return team;
    }

    private void setupScoreBoard() {
        gameScoreBoardObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        gameScoreBoardObjective.setDisplayName(ChatColor.GREEN + "" +  ChatColor.BOLD + " Capture The Flag  ");
        Score score10 = gameScoreBoardObjective.getScore("      "); score10.setScore(10);
        Score score9 = gameScoreBoardObjective.getScore("     "); score9.setScore(9);
        Score score8 = gameScoreBoardObjective.getScore(ChatColor.BLUE + "    Blue " + ChatColor.GOLD + "Team Goal"); score8.setScore(8);
        prevRedFlagScore = gameScoreBoardObjective.getScore(ChatColor.RED + "Flags"); prevRedFlagScore.setScore(7);
        Score score6 = gameScoreBoardObjective.getScore("    "); score6.setScore(6);
        Score score5 = gameScoreBoardObjective.getScore("   "); score5.setScore(5);
        Score score4 = gameScoreBoardObjective.getScore(ChatColor.RED + "    Red " + ChatColor.GOLD + "Team Goal"); score4.setScore(4);
        prevBlueFlagScore = gameScoreBoardObjective.getScore(ChatColor.BLUE + "Flags"); prevBlueFlagScore.setScore(3);
        setFlagScores();
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                  Overrided Methods
----------------------------------------------------------------------------------------------------------------------*/

    @Override
    public boolean sendPlayer(Player player) {
        if (super.sendPlayer(player)) {
            player.getInventory().addItem(SimplePVPCTF.getKits().getKitMenuItem());
            player.getInventory().setItem(7, joinRedTeamItem);
            player.getInventory().setItem(8, joinBlueTeamItem);
            return true;
        }
        return false;
    }

    @Override
    public boolean removePlayer(Player player) {
        // Only run if the player was removed from the game.
        if (super.removePlayer(player)) {
            redTeamPlayers.remove(player);
            blueTeamPlayers.remove(player);

            redTeamPlayerQueue.remove(player);
            blueTeamPlayerQueue.remove(player);

            redTeam.removeEntry(player.getName());
            blueTeam.removeEntry(player.getName());

            if (redFlagCarrier != null && redFlagCarrier.equals(player)) {
                redFlagCarrier = null;
            }

            if (blueFlagCarrier != null && blueFlagCarrier.equals(player)) {
                blueFlagCarrier = null;
            }

            player.setScoreboard(Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard());
            checkGameWin();
            return true;
        }
        return false;
    }

    @Override
    protected void killPlayer(Player player, Player damager, EntityDamageEvent.DamageCause cause) {
        super.killPlayer(player, damager, cause);

        if (player.equals(redFlagCarrier)) {
            returnRedFlag();
        }

        if (player.equals(blueFlagCarrier)) {
            returnBlueFlag();
        }

        if (gameStarted && !gameEnded) {
            player.sendTitle(ChatColor.RED + "You Died!", "You will respawn soon.", 0, 20, 10);
            respawnPlayer(player);
        }

        checkGameWin();
    }

    @Override
    protected void respawnPlayer(Player player) {
        Bukkit.getScheduler().runTaskLater(SimplePVPCTF.getPlugin(), () -> {
            if (gameStarted && !gameEnded) {
                if (redTeamPlayers.contains(player)) {
                    player.teleport(((CTFMapConfig) getMapConfig()).getRedTeamSpawn());
                } else {
                    player.teleport(((CTFMapConfig) getMapConfig()).getBlueTeamSpawn());
                }
                player.setGameMode(GameMode.SURVIVAL);
                SimplePVPCTF.getKits().equipKit(player);
            }
        }, 60);
    }

    @Override
    protected void startGame() {
        int halfTeamSize = getPlayers().size() / 2;

        if (redTeamPlayerQueue.size() > halfTeamSize) {
            redTeamPlayerQueue.subList(halfTeamSize, redTeamPlayerQueue.size()).clear();
        }

        if (blueTeamPlayerQueue.size() > halfTeamSize) {
            blueTeamPlayerQueue.subList(halfTeamSize, blueTeamPlayerQueue.size()).clear();
        }

        for (Player player : redTeamPlayerQueue) {
            redTeamPlayers.add(player);
            redTeam.addEntry(player.getName());
        }

        for (Player player : blueTeamPlayerQueue) {
            blueTeamPlayers.add(player);
            blueTeam.addEntry(player.getName());
        }

        redTeamPlayerQueue.clear();
        blueTeamPlayerQueue.clear();

        for (Player player : getPlayers()) {
            if (!redTeamPlayers.contains(player) && !blueTeamPlayers.contains(player)) {

                if (redTeamPlayers.size() > blueTeamPlayers.size()) {
                    blueTeamPlayers.add(player);
                    blueTeam.addEntry(player.getName());
                } else if (redTeamPlayers.size() < blueTeamPlayers.size()) {
                    redTeamPlayers.add(player);
                    redTeam.addEntry(player.getName());
                } else {
                    if (random.nextBoolean()) {
                        blueTeamPlayers.add(player);
                        blueTeam.addEntry(player.getName());
                    } else {
                        redTeamPlayers.add(player);
                        redTeam.addEntry(player.getName());
                    }
                }
            }
        }


        // Teleport all the players and give them their items.
        for (Player player : getPlayers()) {

            if (redTeamPlayers.contains(player)) {
                player.teleport(((CTFMapConfig) getMapConfig()).getRedTeamSpawn());
            } else {
                player.teleport(((CTFMapConfig) getMapConfig()).getBlueTeamSpawn());
            }

            player.setGameMode(GameMode.SURVIVAL);
            SimplePVPCTF.getKits().equipKit(player);
            player.setScoreboard(gameScoreBoard);
        }

        if (particleTask != null) {
            particleTask.cancel();
        }
        particleTask = Bukkit.getScheduler().runTaskTimer(SimplePVPCTF.getPlugin(), this::trackerParticles, 10, 10);
    }

    @Override
    protected void checkGameWin() {
        super.checkGameWin();
        if (gameStarted && !gameEnded) {
            if (redTeamCaptures >= 3 && blueTeamCaptures >=3) {
                endGame(WinState.TIE);
            } else if (redTeamCaptures >= 3 || blueTeamPlayers.isEmpty()) {
                endGame(WinState.RED);
            } else if (blueTeamCaptures >=3 || redTeamPlayers.isEmpty()) {
                endGame(WinState.BLUE);
            }
        }
    }

    // End the game.
    private void endGame(WinState winState) {
        // Show the winner if
        switch (winState) {
            case RED -> {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.RED + "Red team" + ChatColor.GOLD + " won the game!");
                SimpleUtil.sendPlayersTitle(redTeamPlayers.stream().toList(), ChatColor.GREEN + "You Win!", "", 0, 20, 10);
                SimpleUtil.sendPlayersTitle(blueTeamPlayers.stream().toList(), ChatColor.DARK_GRAY + "You Lost.", "", 0, 20, 10);
            }
            case BLUE -> {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.BLUE + "Blue team" + ChatColor.GOLD + " won the game!");
                SimpleUtil.sendPlayersTitle(blueTeamPlayers.stream().toList(), ChatColor.GREEN + "You Win!", "", 0, 20, 10);
                SimpleUtil.sendPlayersTitle(redTeamPlayers.stream().toList(), ChatColor.DARK_GRAY + "You Lost.", "", 0, 20, 10);
            }
            case TIE -> {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + "The game was a tie!");
                SimpleUtil.sendPlayersTitle(redTeamPlayers.stream().toList(), ChatColor.GOLD + "Tie.", "", 0, 20, 10);
            }
        }

        super.endGame();
    }

    @Override
    protected void resetGame() {
        super.resetGame();

        if (particleTask != null) {
            particleTask.cancel();
        }
        returnRedFlag();
        returnBlueFlag();
        redFlagCarrier = null;
        blueFlagCarrier = null;
        redTeamPlayers.clear();
        blueTeamPlayers.clear();
        redTeamPlayerQueue.clear();
        blueTeamPlayerQueue.clear();
        redTeamCaptures = 0;
        blueTeamCaptures = 0;
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Game Methods
----------------------------------------------------------------------------------------------------------------------*/

    private void pickupRedFlag(Player player) {
        if (redFlagCarrier == null && blueTeamPlayers.contains(player)) {
            redFlagCarrier = player;
            player.getInventory().setHelmet(new ItemStack(Material.RED_BANNER));
            ctfMapConfig.getRedFlag().setType(Material.AIR);
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + player.getName() + " has picked up the " + ChatColor.RED + "red" + ChatColor.GOLD + " team's flag.");
            SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.ENTITY_BAT_TAKEOFF, 0.75f);
            setFlagScores();
        }
    }

    private void pickupBlueFlag(Player player) {
        if (blueFlagCarrier == null && redTeamPlayers.contains(player)) {
            blueFlagCarrier = player;
            player.getInventory().setHelmet(new ItemStack(Material.BLUE_BANNER));
            ctfMapConfig.getBlueFlag().setType(Material.AIR);
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + player.getName() + " has picked up the " + ChatColor.BLUE + "blue" + ChatColor.GOLD + " team's flag.");
            SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.ENTITY_BAT_TAKEOFF, 0.75f);
            setFlagScores();
        }
    }

    private void captureRedFlag(Player player) {
        if (redFlagCarrier == player) {
            redFlagCarrier.getInventory().setHelmet(new ItemStack(Material.AIR));
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + redFlagCarrier.getName() + " has captured " + ChatColor.RED + "red" + ChatColor.GOLD + " team's flag.");
            SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.ENTITY_WITHER_DEATH, 1f);
            redFlagCarrier = null;
            returnRedFlag();
            blueTeamCaptures++;
            setFlagScores();
            checkGameWin();
        }
    }

    private void captureBlueFlag(Player player) {
        if (blueFlagCarrier == player) {
            blueFlagCarrier.getInventory().setHelmet(new ItemStack(Material.AIR));
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + blueFlagCarrier.getName() + " has captured " + ChatColor.BLUE + "blue" + ChatColor.GOLD + " team's flag.");
            SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.ENTITY_WITHER_DEATH, 1f);
            blueFlagCarrier = null;
            returnBlueFlag();
            redTeamCaptures++;
            setFlagScores();
            checkGameWin();
        }
    }

    private void returnRedFlag() {
        if (redFlagCarrier != null) {
            redFlagCarrier.getInventory().setHelmet(new ItemStack(Material.AIR));
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + redFlagCarrier.getName() + " has dropped " + ChatColor.RED + "red" + ChatColor.GOLD + " team's flag.");
            SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.BLOCK_GRAVEL_STEP, 0.75f);
            redFlagCarrier = null;
        }

        Block redFlag = ctfMapConfig.getRedFlag();
        redFlag.setType(Material.RED_BANNER);
        Rotatable redRotatable = (Rotatable) redFlag.getBlockData();
        redRotatable.setRotation(ctfMapConfig.getRedFlagDirection());
        redFlag.setBlockData(redRotatable);
        setFlagScores();
    }

    private void returnBlueFlag() {
        if (blueFlagCarrier != null) {
            blueFlagCarrier.getInventory().setHelmet(new ItemStack(Material.AIR));
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + blueFlagCarrier.getName() + " has dropped " + ChatColor.BLUE + "blue" + ChatColor.GOLD + " team's flag.");
            SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.BLOCK_GRAVEL_STEP, 0.75f);
            blueFlagCarrier = null;
        }

        Block blueFlag = ctfMapConfig.getBlueFlag();
        blueFlag.setType(Material.BLUE_BANNER);
        Rotatable blueRotatable = (Rotatable) blueFlag.getBlockData();
        blueRotatable.setRotation(ctfMapConfig.getBlueFlagDirection());
        blueFlag.setBlockData(blueRotatable);
        setFlagScores();
    }

    private void trackerParticles() {
        if (redFlagCarrier != null) {
            Location location = redFlagCarrier.getLocation();
            Particle.DustOptions dust = new Particle.DustOptions(
                    Color.fromRGB(255, 0, 0), 1);
            for (int i = 3; i < 20; i++) {
                Objects.requireNonNull(location.getWorld()).spawnParticle(Particle.REDSTONE,
                        location.getX(), location.getY() + i, location.getZ(), 2, 0, 0, 0,0 ,dust);
            }
        }

        if (blueFlagCarrier != null) {
            Location location = blueFlagCarrier.getLocation();
            Particle.DustOptions dust = new Particle.DustOptions(
                    Color.fromRGB(0, 0, 255), 1);
            for (int i = 3; i < 20; i++) {
                Objects.requireNonNull(location.getWorld()).spawnParticle(Particle.REDSTONE,
                        location.getX(), location.getY() + i, location.getZ(), 2, 0, 0, 0,0 ,dust);
            }
        }
    }

    public void setFlagScores() {
        int scoreIndex = 0;
        if (prevRedFlagScore != null) {
            scoreIndex = prevRedFlagScore.getScore();
            gameScoreBoard.resetScores(prevRedFlagScore.getEntry());
        }

        StringBuilder sb = new StringBuilder(ChatColor.GRAY  + "     Flags:" + ChatColor.RED);

        for (int i = 0; i < 3; i++) {
            if (i+1 <= blueTeamCaptures) {
                sb.append(" ⬛");
            } else if (i+1 <= blueTeamCaptures + 1 && redFlagCarrier != null){
                sb.append(" ▒");
            } else {
                sb.append(" ⬜");
            }
        }
        prevRedFlagScore = gameScoreBoardObjective.getScore(sb.toString());
        prevRedFlagScore.setScore(scoreIndex);

        scoreIndex = 0;
        if (prevBlueFlagScore != null) {
            scoreIndex = prevBlueFlagScore.getScore();
            gameScoreBoard.resetScores(prevBlueFlagScore.getEntry());
        }

        sb = new StringBuilder(ChatColor.GRAY  + "     Flags:" + ChatColor.BLUE);

        for (int i = 0; i < 3; i++) {
            if (i+1 <= redTeamCaptures) {
                sb.append(" ⬛");
            } else if (i+1 <= redTeamCaptures + 1 && blueFlagCarrier != null){
                sb.append(" ▒");
            } else {
                sb.append(" ⬜");
            }
        }
        prevBlueFlagScore = gameScoreBoardObjective.getScore(sb.toString());
        prevBlueFlagScore.setScore(scoreIndex);
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Game Events
----------------------------------------------------------------------------------------------------------------------*/
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (e.getPlayer().equals(redFlagCarrier)) {
            Location location = e.getPlayer().getLocation();
            Particle.DustOptions dust = new Particle.DustOptions(
                    Color.fromRGB(255, 0, 0), 1);
            for (int i = 3; i < 20; i++) {
                Objects.requireNonNull(location.getWorld()).spawnParticle(Particle.REDSTONE,
                        location.getX(), location.getY() + i, location.getZ(), 0, 0, 0, 0, dust);
            }
        }
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player player) {

            // Prevent friendly fire
            if (e.getDamager() instanceof Player damager) {
                if (redTeamPlayers.contains(player) && redTeamPlayers.contains(damager)) {
                    e.setCancelled(true);
                } else if (blueTeamPlayers.contains(player) && blueTeamPlayers.contains(damager)) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (hasPlayer(e.getPlayer())) {
            if (gameStarted && !gameEnded && e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                Block clickedBlock = e.getClickedBlock();

                if (clickedBlock != null) {
                    if (clickedBlock.equals(ctfMapConfig.getRedFlagBase()) || clickedBlock.equals(ctfMapConfig.getRedFlag())) {
                        pickupRedFlag(e.getPlayer());
                        captureBlueFlag(e.getPlayer());
                    } else if (clickedBlock.equals(ctfMapConfig.getBlueFlagBase()) || clickedBlock.equals(ctfMapConfig.getBlueFlag())) {
                        pickupBlueFlag(e.getPlayer());
                        captureRedFlag(e.getPlayer());
                    }
                }
            }

            if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
                ItemStack item = e.getItem();
                if (item != null) {
                    if (item.equals(joinRedTeamItem)) {
                        if (redTeamPlayerQueue.contains(e.getPlayer())) {
                            e.getPlayer().sendMessage(ChatColor.DARK_RED + "You are already queued for this team.");
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_DISPENSER_DISPENSE, Float.MAX_VALUE, 0);
                        } else {
                            e.getPlayer().sendMessage(ChatColor.GOLD + "Adding you to the " + ChatColor.RED + "red" + ChatColor.GOLD +" team queue.");
                            blueTeamPlayerQueue.remove(e.getPlayer());
                            redTeamPlayerQueue.add(e.getPlayer());
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_DISPENSER_DISPENSE, Float.MAX_VALUE, 2);
                        }
                        e.setCancelled(true);
                    } else if (item.equals(joinBlueTeamItem)) {
                        if (blueTeamPlayerQueue.contains(e.getPlayer())) {
                            e.getPlayer().sendMessage(ChatColor.DARK_RED + "You are already queued for this team.");
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_DISPENSER_DISPENSE, Float.MAX_VALUE, 0);
                        } else {
                            e.getPlayer().sendMessage(ChatColor.GOLD + "Adding you to the " + ChatColor.BLUE + "blue" + ChatColor.GOLD +" team queue.");
                            redTeamPlayerQueue.remove(e.getPlayer());
                            blueTeamPlayerQueue.add(e.getPlayer());
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_DISPENSER_DISPENSE, Float.MAX_VALUE, 2);
                        }
                        e.setCancelled(true);
                    }
                }
            }
        }
    }
}

