package com.simplepvpctf;

import com.simplepvp.MapConfig;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

public class CTFMapConfig extends MapConfig {
    private final Location redTeamSpawn;
    private final Location blueTeamSpawn;
    private final BlockFace redFlagDirection;
    private final BlockFace blueFlagDirection;
    private final Block redFlagBase;
    private final Block blueFlagBase;
    private final Block redFlag;
    private final Block blueFlag;

    public CTFMapConfig(ConfigurationSection cs) {
        // Call the super.
        super(convertToTitleCase(cs.getName().replace('-', ' ')),
                cs.getInt("min-players"), cs.getInt("max-players"), cs.getLocation("spawn-location"));

        redTeamSpawn = cs.getLocation("red-team-spawn");
        blueTeamSpawn = cs.getLocation("blue-team-spawn");


        if (cs.getString("red-team-flag-direction") == null || !(Objects.requireNonNull(cs.getString("red-team-flag-direction")).equalsIgnoreCase("north")
                || Objects.requireNonNull(cs.getString("red-team-flag-direction")).equalsIgnoreCase("south")
                || Objects.requireNonNull(cs.getString("red-team-flag-direction")).equalsIgnoreCase("east")
                || Objects.requireNonNull(cs.getString("red-team-flag-direction")).equalsIgnoreCase("west"))) {
            cs.set("red-team-flag-direction", "north");
        }

        redFlagDirection = BlockFace.valueOf(cs.getString("red-team-flag-direction"));

        if (cs.getString("blue-team-flag-direction") == null || !(Objects.requireNonNull(cs.getString("blue-team-flag-direction")).equalsIgnoreCase("north")
                || Objects.requireNonNull(cs.getString("blue-team-flag-direction")).equalsIgnoreCase("south")
                || Objects.requireNonNull(cs.getString("blue-team-flag-direction")).equalsIgnoreCase("east")
                || Objects.requireNonNull(cs.getString("blue-team-flag-direction")).equalsIgnoreCase("west"))) {
            cs.set("blue-team-flag-direction", "north");
        }

        blueFlagDirection = BlockFace.valueOf(cs.getString("blue-team-flag-direction"));

        redFlagBase = (getSpawnLocation().getWorld()).getBlockAt(
                cs.getInt("red-team-flag-base.x"), cs.getInt("red-team-flag-base.y"), cs.getInt("red-team-flag-base.z"));
        blueFlagBase = (getSpawnLocation().getWorld()).getBlockAt(
                cs.getInt("blue-team-flag-base.x"), cs.getInt("blue-team-flag-base.y"), cs.getInt("blue-team-flag-base.z"));

        redFlag = redFlagBase.getRelative(BlockFace.UP);
        blueFlag = blueFlagBase.getRelative(BlockFace.UP);
    }

    // Used to convert strings to title case. Used for map names.
    public static String convertToTitleCase(String text) {
        StringBuilder converted = new StringBuilder();

        boolean upperCaseNext = true;
        for (char c : text.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                upperCaseNext  = true;
            } else if (upperCaseNext) {
                c = Character.toUpperCase(c);
                upperCaseNext  = false;
            } else {
                c = Character.toLowerCase(c);
            }
            converted.append(c);
        }

        return converted.toString();
    }

    // Getters.
    public Location getRedTeamSpawn() {
        return redTeamSpawn;
    }

    public Location getBlueTeamSpawn() {
        return blueTeamSpawn;
    }

    public Block getRedFlagBase() {
        return redFlagBase;
    }

    public Block getBlueFlagBase() {
        return blueFlagBase;
    }

    public Block getRedFlag() {
        return redFlag;
    }

    public Block getBlueFlag() {
        return blueFlag;
    }

    public BlockFace getRedFlagDirection() {
        return redFlagDirection;
    }

    public BlockFace getBlueFlagDirection() {
        return blueFlagDirection;
    }
}
